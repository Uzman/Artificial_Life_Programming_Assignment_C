/**
 * Created by oguz on 19/05/16.
 */
public class AssignmentC {
    public static void main(String[] args) {
        double a, b, c, d, e, f;
        double initialX = 500, initialY = 500;
        a = -0.01;
        b = 0.010;
        c = -0.008;
        d = 0.012;
        e = - 0.00001;
        f = -0.0001;
        /*
        a = -0.01;
        b = 0.010;
        c = -0.008;
        d = 0.012;
        e = - 0.0000;
        f = -0.00001;*/
        /*
        a = -0.01;
        b = 0.010;
        c = -0.008;
        d = 0.018;
        e = - 0.0000;
        f = -0.00001;*/
        startSimulation(initialX, initialY, a, b, c, d, e, f, 2000, true);
    }


    private static void startSimulation(double initialX, double initialY, double a, double b, double c, double d,
                                        double e, double f, int timeStepCount, boolean printResultsToConsole) {
        double currentX = initialX;
        double currentY = initialY;
        for (int i = 0; i < timeStepCount; i++) {
            if(printResultsToConsole) {
                printState(i, currentX, currentY);
            }
            /**
             * x(i+1) = x(i)+a∗x(i)+b∗y(i)+e∗x(i)∗x(i)
             * y(i+1) = y(i)+c∗x(i)+d∗y(i)+f ∗y(i)∗y(i)
             */

            double newX = currentX + a * currentX + b * currentY + e * currentX * currentX;
            double newY = currentY + c * currentX + d * currentY + f * currentY * currentY;

            currentX = newX;
            currentY = newY;

        }
        if(printResultsToConsole) {
            printState(timeStepCount, currentX, currentY);
        }
    }

    private static void printState(int step, double x, double y) {
        System.out.println("Step " + step + ": " + x + ", " + y);
    }
}
